/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./public/*.{html,js}", "./src/**/*.{html,js}"],
  theme: {
    extend: {},
    container: {
      center: true,
      padding: 20,
    },
  },
  plugins: [require("daisyui")],
};
