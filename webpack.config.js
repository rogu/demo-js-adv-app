const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { ModuleFederationPlugin } = require("webpack").container;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: ["./src/bootstrap.js", "./src/style/app.scss"],
  target: "web",
  mode: "development",
  devtool: "inline-source-map",
  watch: true,
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "public/dist"),
    publicPath: "auto",
  },
  devServer: {
    port: 2345,
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.(scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "postcss-loader",
          "sass-loader",
        ],
      },
      {
        test: /\.css$/i,
        use: [
          "css-loader",
          "postcss-loader",
          "sass-loader"
        ],
      },
      {
        test: /\.html$/i,
        loader: "html-loader",
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
    }),
    new ModuleFederationPlugin({
      remotes: {
        debuggerLib: `lib@https://lib.debugger.pl/remoteEntry.js`,
      },
    }),
    new HtmlWebpackPlugin({
      template: 'src/assets/layout.html'
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from: 'src/assets/static' }
      ]
    })
  ],
};
