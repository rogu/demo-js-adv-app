import { Store } from "../..";
import { TYPES } from "../../services";
import { Helpers } from "../../utils";

export function addItemToCart(item) {
    return (state) => {
        const cart = Helpers.increaseParamValue(state.cart, item, "count");
        return { ...state, cart };
    };
}

export function removeItemFromCart(item) {
    return (state) => {
        const cart = Helpers.decreaseParamValue(state.cart, item, "count");
        return { ...state, cart };
    };
}

export function addItemTotalPrice(state) {
    const cart = state.cart.map((item) => ({
        ...item,
        totalPrice: +(item.price * item.count),
    }));
    return { ...state, cart };
}

export function addNettoPrice(state) {
    const updatedCart = state.cart.map((item) => ({
        ...item,
        nettoPrice: +(item.price / state.tax).toFixed(2),
    }));
    return { ...state, cart: updatedCart };
}

export function addTotalPrice(state) {
    const totalCart = state.cart.reduce((acc, item) => acc + item.price * item.count, 0);
    return { ...state, totalCart };
}

export function mergeCartWithShopping(state) {
    const merged = state.shopping.map((item) => {
        const exists = state.cart.find((x) => item.id === x.id);
        return exists
            ? {
                ...item,
                count: exists.count + item.count,
                totalPrice: +(exists.totalPrice + item.totalPrice).toFixed(2),
                nettoPrice: +(exists.nettoPrice + item.nettoPrice).toFixed(2),
            }
            : item;
    });
    const notMerged = state.cart.filter(
        (item) => !merged.find((x) => item.id === x.id)
    );
    return { ...state, shopping: [...merged, ...notMerged] };
}

export function clearCart(state) {
    return { ...state, cart: [], totalCart: 0 };
}

export function applyDiscount(state) {
    return { ...state, totalShopping: +(state.totalShopping * state.discount).toFixed(2) };
}

export function calcTotalPrice(state) {
    const totalShopping = state.shopping.reduce((acc, item) => acc + item.price * item.count, 0);
    return { ...state, totalShopping };
}

export function updateStore(state) {
    Store.dispatch({ type: TYPES.SET_CART, payload: state });
    return state;
}
