import { CartService } from "..";
import { Store } from "../..";
import { Component, Events, Helpers } from "../../utils";
import tpl from './cart.container.html';

@Component({ tpl })
export class CartContainer extends HTMLElement {
  constructor() {
    super();
    Store
      .pipe(
        (store) => store.cart
      )
      .subscribe(this.rerender.bind(this));
  }

  rerender({ cart, shopping, totalShopping, totalCart, discount, tax }) {
    const config = [
      { key: "title" },
      { key: 'nettoPrice' },
      { key: "price" },
      { key: "totalPrice" },
      { key: "count" }];
    this.ui.cart.render(cart, config);
    this.ui.shopping.render(shopping, config);
    this.ui.buyBtn.classList[!cart.length ? 'add' : 'remove']("hidden");
    this.ui.infoShopping.innerHTML = `
            Total (discount ${(1 - discount).toFixed(1) * 100}%, tax ${String(tax).substring(2)}%): ${totalShopping} zł
    `;
    this.ui.infoCart.innerHTML = `
            Total (without discount, tax ${String(tax).substring(2)}%): ${totalCart} zł
    `;
  }

  connectedCallback() {
    this.ui = Helpers.uiHandler(
      ["#cart", Events.REMOVE, ({ detail: id }) => CartService.remove(id)],
      ["#cart", Events.ADD, ({ detail }) => {
        const found = Store.getValue().cart.cart.find(({ id }) => id === detail);
        CartService.add(found);
      }],
      ["#shopping"],
      ["#buy-btn",
        Events.CLICK,
        ({ target }) => {
          target.remove();
          CartService.buy();
        },
      ],
      [".info-cart"],
      [".info-shopping"]
    );
  }
}

customElements.define("ui-cart", CartContainer);
