import { Store } from "../..";
import { HttpBaseService, TYPES, idbGetCart, idbSetCart } from "../../services";
import { API, Helpers } from "../../utils";
import {
  addItemToCart,
  addItemTotalPrice,
  applyDiscount,
  mergeCartWithShopping,
  calcTotalPrice,
  clearCart,
  removeItemFromCart,
  updateStore,
  addNettoPrice,
  addTotalPrice
} from "./cart.utils";

class Cart extends HttpBaseService {
  constructor() {
    super(API.MANDATORY);
  }
  async initCart() {
    const payload = (await idbGetCart()) || [];
    Store
      .pipe(
        (store) => store.cart
      )
      .subscribe((data) => idbSetCart(data));
    Store.dispatch({ type: TYPES.SET_CART, payload });
    !payload.cart?.length && !payload.shopping?.length && instance.setMandatoryItems();
  }

  async setMandatoryItems() {
    const { data } = await super.fetch();
    data.forEach((val) => this.add(val));
  }

  add(item) {
    const state = Store.getValue().cart;
    Helpers.pipe(
      addItemToCart(item),
      addItemTotalPrice,
      addNettoPrice,
      addTotalPrice,
      updateStore
    )(state);
  }

  remove(id) {
    const state = Store.getValue().cart;
    const toRemove = state.cart.find((item) => item.id === id);
    Helpers.pipe(
      removeItemFromCart(toRemove),
      addItemTotalPrice,
      addNettoPrice,
      addTotalPrice,
      updateStore
    )(state);
  }

  buy() {
    const state = Store.getValue().cart;
    Helpers.pipe(
      mergeCartWithShopping,
      clearCart,
      calcTotalPrice,
      applyDiscount,
      updateStore
    )(state);
  }
}

const instance = new Cart();

export default instance;
