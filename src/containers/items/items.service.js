import { HttpBaseService, TYPES } from "../../services";
import { Store } from "../..";
import { API } from "../../utils";

class ItemsService extends HttpBaseService {

    constructor() {
        super(API.ITEMS);
    }

    async fetch(filters) {
        const resp = await super.fetch(filters);
        Store.dispatch({ type: TYPES.SET_ITEMS, payload: resp });
    }
}

const instance = new ItemsService();

export default instance;
