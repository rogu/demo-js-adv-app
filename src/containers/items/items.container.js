import { Component, Events, Helpers } from "../../utils";
import { CartService, ItemsService } from "..";
import { Store } from "../..";
import tpl from './items.container.html';

@Component({ tpl })
export class ItemsContainer extends HTMLElement {
  constructor() {
    super();
    this.filters = {
      itemsPerPage: 5,
      currentPage: 1,
    };
    const config = [
      { key: "title" },
      { key: "price", type: "input" },
      { key: "imgSrc", type: "image" },
    ];
    Store
      .pipe(
        (store) => store.items
      )
      .subscribe(({ data, entities, total }) => {
        this.data = data;
        this.entities = entities;
        this.ui.uiDatagrid.render(data, config);
        this.ui.uiPagination.render({ ...this.filters, total });
      });

    ItemsService.fetch(this.filters);
  }

  connectedCallback() {
    this.ui = Helpers.uiHandler(
      [
        "ui-search",
        Events.SEARCH,
        ({ detail: { id, value } }) =>
          this.updateFilters({ [id]: value }),
      ],
      [
        "ui-search",
        Events.SEARCH_RESET,
        ({ detail: value }) =>
          this.updateFilters(value),
      ],
      [
        "ui-datagrid",
        Events.MORE,
        ({ detail: id }) => this.ui.uiDetails.render(this.entities[id]),
      ],
      [
        "ui-datagrid",
        Events.BUY,
        ({ detail: id }) => {
          Helpers.loading1s();
          CartService.add(this.entities[id]);
        },
      ],
      ["ui-details"],
      [
        "ui-pagination",
        Events.CHANGE,
        ({ detail }) => this.updateFilters({ currentPage: detail }),
      ]
    );
  }

  updateFilters(param) {
    this.filters = {
      ...this.filters,
      ...param,
      ...(!Object.keys(param).includes("currentPage") && { currentPage: 1 }),
    };
    ItemsService.fetch(this.filters);
  }
}

customElements.define("ui-items", ItemsContainer);
