import { Store } from "../..";
import { HttpBaseService, TYPES } from "../../services";
import { API } from "../../utils";

class WorkersService extends HttpBaseService {

    constructor() {
        super(API.WORKERS);
    }

    async fetch() {
        const { data } = await super.fetch()
        Store.dispatch({ type: TYPES.SET_WORKERS, payload: data });
    }
}

const instance = new WorkersService();

export default instance;
