import { Helpers, Events, Component } from "../../utils";
import { WorkersService } from "..";
import { Store } from "../..";
import tpl from './workers.container.html';

@Component({ tpl })
export class WorkersContainer extends HTMLElement {
  constructor() {
    super();
    const config = [
      { key: "name" },
      { key: "phone", type: "input" },
      { key: "category" },
    ];
    Store
      .pipe(store => store.workers)
      .subscribe(({ data }) => this.ui.uiDatagrid.render(data, config));
    WorkersService.fetch();
  }

  connectedCallback() {
    this.ui = Helpers.uiHandler(
      ["ui-details"],
      [
        "ui-datagrid",
        Events.MORE,
        ({ detail: id }) => this.ui.uiDetails.render(Store.getValue().workers.entities[id]),
      ]);
  }

}

customElements.define("ui-workers", WorkersContainer);
