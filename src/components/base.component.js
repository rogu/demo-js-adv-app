export default class BaseComponent extends HTMLElement {
    constructor() {
        super();
    }

    renderContent(id) {
        const tpl = this.querySelector('template');
        if (!tpl) return;
        const content = tpl.content.cloneNode(true);
        return Array.from(content.childNodes)
            .filter((el) => el.nodeType === 1)
            .map((el) => {
                el.setAttribute('data-id', id);
                return el.outerHTML;
            })
            .join('');
    }

    get attrsReady() {
        return new Promise((res, rej) => {
            const err = this.constructor.observedAttributes.reduce((acc, el) =>
                ({ ...acc, ...(!this.hasAttribute(el) && { [el]: `missing attr ${el}` }) }), {})
            return Reflect.ownKeys(err).length
                ? rej(err)
                : res()
        })
    }

}
