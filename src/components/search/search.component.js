import { Helpers, Events } from "../../utils";

export default class SearchComponent extends HTMLElement {

    updateRangeLabel(value) {
        this.ui.rangeValue.innerHTML = value;
    }

    connectedCallback() {
        const box = document.createElement('div');
        this.content = document.createElement('div');
        box.appendChild(this.content);
        this.appendChild(box);

        this.content.innerHTML = `
        <div class="box">
                <div class="title">Search</div>
                <form id="search-form" class="flex flex-col gap-4">
                    <div>
                        <input id="title" class="input input-bordered input-sm" placeholder="search by title" type="text"/>
                    </div>

                    <div>
                        <label for="priceFrom">by price from <span class="range-value">0</span></label>
                        <br />
                        <input type="range" value="0" id="priceFrom" class="input w-full max-w-xs">
                    </div>

                    <div>
                        <label for="items-per-page">Items Per Page</label>
                        <br />
                        <ui-limiter id="items-per-page" selected='5' data='[2,5,10]'></ui-limiter>
                    </div>
                    <button class="btn btn-sm" type="reset">reset</button>
                </form>
        </div>`;

        this.ui = Helpers.uiHandler(
            ['.range-value'],
            ['#search-form', Events.INPUT, ({ target: { id, value } }) => {
                if (id === 'priceFrom') this.updateRangeLabel(value);
                Helpers.delay(() => {
                    this.dispatchEvent(new CustomEvent(Events.SEARCH, { detail: { id, value } }))
                }, 500);
            }],
            ['#search-form', "reset", ({ target }) => {
                setTimeout(() => {
                    const formState = Object.entries(target)
                        .filter(([, el]) => el.tagName !== 'BUTTON')
                        .map(([, el]) => ({ [Helpers.dashToCamelCase(el.id)]: el.value }))
                        .reduce((acc, field) => ({ ...acc, ...field }), {})
                    this.updateRangeLabel(0);
                    this.dispatchEvent(new CustomEvent(Events.SEARCH_RESET, { detail: formState }));
                });
            }]
        )

    }
}

customElements.define('ui-search', SearchComponent);
