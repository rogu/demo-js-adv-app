import CSS from './win.css';
const stylesheet = new CSSStyleSheet();

let counter = 0;

function validatePosition(pos) {
    return Math.max(0, Math.round(pos));
}

const noop = () => { };
function createAliasProperty(prop, validate = String, onChange = noop) {
    return {
        enumerable: true,
        get() {
            return validate(this.getAttribute(prop));
        },
        set(value) {
            this.setAttribute(prop, value);
            onChange.call(this, prop, validate(value));
        }
    };
}

function updateAfterValidate(prop, value) {
    this.style.setProperty('--' + prop, value + 'px');
}

const config = {
    y: [validatePosition, updateAfterValidate],
    x: [(pos) => Math.max(0, pos), updateAfterValidate],
    autofocus: [Boolean, noop],
    maximized: [Boolean, noop]
};

let zCount = 0;

let cur;
class MdiWindow extends HTMLElement {
    static get observedAttributes() {
        return ['width', 'height', 'x', 'y', 'autofocus', 'rolledup', 'maximized'];
    }

    constructor() {
        super();
        this._id = ++counter;

        const attrs = this.constructor.observedAttributes;

        for (let i = 0; i < attrs.length; i++) {
            const attr = attrs[i];
            const conf = config[attr] || [Math.round, updateAfterValidate];
            const desc = createAliasProperty(attr, conf[0], conf[1]);

            Object.defineProperty(this, attr, desc);
            if (this.hasAttribute(attr)) {
                desc.set.call(this, this.getAttribute(attr));
            }
        }

        const shadow = this.attachShadow({ mode: 'open' });
        shadow.adoptedStyleSheets = [stylesheet];
        shadow.innerHTML = `
            <div class="x">x</div>
            <header>
                <slot name="title">win ${this._id}</slot>
            </header>
            <section>
                <slot></slot>
            </section>
            <footer>
                <slot name="footer"></slot>
            </footer>
            `.replace(/[\s\n]*\n[\s\n]*/g, '');

        const header = this.shadowRoot.firstChild;
        const preventDefault = e => {
            e.preventDefault();
            e.stopPropagation();
        };
        shadow.querySelector('.x').addEventListener('click', (ev)=>{
            this.parentNode.removeChild(this);
        })
        header.addEventListener('touchstart', preventDefault);
        header.addEventListener('dragstart', preventDefault);
        header.addEventListener('selectstart', preventDefault);
        const type = 'onpointerdown' in this ? 'pointer' : 'mouse';
        this.addEventListener(type + 'down', e => {
            this.focus();
            let start, x, y;
            const move = e => {
                if (!start) {
                    x = this.x;
                    y = this.y;
                    start = e;
                    return;
                }
                this.x = x + e.pageX - start.pageX;
                this.y = y + e.pageY - start.pageY;
                e.preventDefault();
                e.stopPropagation();
            };
            const up = e => {
                removeEventListener(type + 'move', move, true);
                removeEventListener(type + 'up', up, true);
                if (start) move(e);
            };
            addEventListener(type + 'move', move, true);
            addEventListener(type + 'up', up, true);
            preventDefault(e);
            return false;
        });
    }

    connectedCallback() {
        this.tabIndex = -1;
        // Only actually parse the stylesheet when the first instance is connected.
        if (stylesheet.cssRules.length == 0) {
            stylesheet.replaceSync(CSS);
        }
        if (this.autofocus) this.focus();
    }

    focus() {
        super.focus();
        this.style.zIndex = ++zCount;
    }
}

customElements.define('mdi-window', MdiWindow);

let x = 0;
window.newwin.addEventListener('click', () => {
    const offset = x++ % 10;
    const win = document.createElement('mdi-window');
    win.x = 100 + 20 * offset;
    win.y = 20 * offset;
    win.width = win.height = 100;
    win.autofocus = true;
    document.body.appendChild(win);
});
