export class DetailsComponent extends HTMLElement {

    connectedCallback() {
        this.root = this.attachShadow({ mode: 'open' });

        this.root.innerHTML = `
                <style>
                    dt {
                        font-weight: bold;
                    }
                </style>
                <slot name="title"></slot>
                <div class="detail">nothing to show</div>
            `
    }

    render(data) {
        this.root.querySelector('.detail').innerHTML = `
                <dl>
                    ${Object.keys(data).map((key) => {
            return `
                        <dt>${key}</dt>
                        <dd>
                            ${key === 'imgSrc'
                    ? `<img src="${data[key]}" width="50"/>`
                    : `${data[key]}`
                }
                        </dd>
                    `}).join('')}
                </dl>
            `;
    }
}
customElements.define('ui-details', DetailsComponent);
