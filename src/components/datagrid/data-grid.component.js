import tpl from "./data-grid.component.html";
import { Component, Events } from "../../utils";
import BaseComponent from "../base.component";

@Component({ tpl })
export class DataGridComponent extends BaseComponent {

    connectedCallback() {
        // nasłuchujemy na kliknięcie w cały element. W funkcji clickHandler sprawdzamy co zostało kliknięte.
        this.addEventListener(Events.CLICK, this.clickHandler);
    }

    disconnectedCallback() {
        this.removeEventListener(Events.CLICK, this.clickHandler);
    }

    attributeChangedCallback(attr, old, value) {
        this[attr] = JSON.parse(value);
    }

    createTag(type, value) {
        switch (type) {
            case 'input':
                return `<input class="form-control form-control-sm" type="text" value="${value}"/>`
            case 'image':
                return `<img src="${value}" class="h-10"/>`
            default:
                return value;
        }
    }

    clickHandler({ target: { innerText: eventName, dataset: { id } } }) {
        id && this.dispatchEvent(new CustomEvent(eventName.toUpperCase(), { detail: id }));
    }

    render(data,config) {
        this.querySelector('.body').innerHTML = `
            ${!data.length
                ? `<div class="p-4 text-gray-500">nothing to show</div>`
                : `<table class="table table-compact">
                        <thead>
                            <tr>
                                <th class="capitalize">Nr.</th>
                                ${config
                    .map(
                        (head) =>
                            `<th class="capitalize">${head.key}</th>`
                    )
                    .join("")}
                                ${this.hasContent ? "<th class='capitalize'>Actions</th>" : ""}
                            </tr>
                        </thead>
                        <tbody>
                            ${data
                    .map(
                        (row, nr) => `<tr>
                                <td>${nr + 1}</td>
                                ${config
                                .map(
                                    ({ type, key }) =>
                                        `<td>${this.createTag(
                                            type,
                                            row[key]
                                        )}</td>`
                                )
                                .join("")}
                                ${this.hasContent
                                ? `<td>
                                    ${this.renderContent(row.id)}
                                </td>`
                                : ""}
                            </tr>`
                    )
                    .join("")}
                        </tbody>
                    </table>`
            }
        `;
    }
}

customElements.define('ui-datagrid', DataGridComponent);
