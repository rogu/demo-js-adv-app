import { Events } from "../../utils";

class Pagination extends HTMLElement {
  constructor() {
    super();
  }
  connectedCallback() {
    this.addEventListener(Events.CLICK, ({ target }) => {
      if (target.tagName === "SPAN") {
        this.querySelector(".btn-active").classList.remove("btn-active");
        target.classList.add("btn-active");
        this.dispatchEvent(
          new CustomEvent(Events.CHANGE, { detail: target.innerText })
        );
      }
    });
  }

  render({ total, itemsPerPage, currentPage }) {
    const pages = Math.ceil(total / itemsPerPage);
    let bricks = pages ? new Array(pages).join().split(",") : [];
    this.innerHTML = `<div>
            ${bricks
        .map((item, idx) => {
          return `<span class="btn btn-sm mr-1 ${currentPage - 1 === idx ? "btn-active" : ""
            }">${idx + 1}</span>`;
        })
        .join("")}
            <div>total: ${total}</div>
        </div>
        `;
  }
}

window.customElements.define("ui-pagination", Pagination);
