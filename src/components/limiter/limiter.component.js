import BaseComponent from "../base.component";

export class LimiterComponent extends BaseComponent {

    static get observedAttributes() {
        return ['data', 'selected']
    }

    async connectedCallback() {
        await this.attrsReady;
        this.render();
    }

    attributeChangedCallback(attr, old, value) {
        this[attr] = JSON.parse(value);
    }

    render() {
        this.innerHTML = `
                <select class="select select-bordered select-sm" id="itemsPerPage">
                    ${this.data.map((opt) => `
                        <option ${opt == this.selected && 'selected'}>
                            ${opt}
                        </option>`)}
                </select>
            `
    }
}

customElements.define('ui-limiter', LimiterComponent);
