import { AuthService } from "../../services";
import { Component, Helpers, Events } from "../../../src/utils";
import tpl from "./auth.component.html";

@Component({ tpl })
export class AuthComponent extends HTMLElement {

    connectedCallback() {
        this.ui = Helpers.uiHandler(
            ['.auth-form', Events.SUBMIT, (ev) => this.login(ev)],
            ['.btn-logout', Events.CLICK, () => this.logout()]);
        this.render();
    }

    async render() {
        const loggedIn = await AuthService.isLogged();
        this.ui.btnLogout.classList.toggle('hidden', !loggedIn);
        this.ui.authForm.classList.toggle('hidden', loggedIn);
    }

    logout() {
        AuthService.logout();
        this.render();
    }

    async login(ev) {
        ev.preventDefault();
        const ui = Helpers.uiHandler(['#username'], ['#password']);
        const [username, password] = [ui.username.value, ui.password.value];
        await AuthService.login({ username, password });
        this.render();
    }
}

customElements.define('ui-auth', AuthComponent);

