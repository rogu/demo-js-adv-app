import tpl from "./spinner.component.html";
import { Component, Helpers, Events } from "../../utils";
import BaseComponent from "../base.component";

@Component({ tpl })
export class SpinnerComponent extends BaseComponent {

    connectedCallback() {
        this.ui = Helpers.uiHandler(
            ["body", Events.LOADING_START, ({ detail }) => this.setLoading(true, detail)],
            ["body", Events.LOADING_STOP, ({ detail }) => this.setLoading(false, detail)],
            [".loading"]
        );
        this.state = this.loading();
    }

    disconnectedCallback() { }

    setLoading(value, key) {
        this.state[value ? 'start' : 'stop'](key);
        this.ui.loading.classList.toggle("invisible", !value);
        this.ui.loading.classList.toggle("opacity-0", !value);
    }

    loading() {
        const dots = ["⠟", "⠻", "⠽", "⠾", "⠷", "⠯"];
        let i = 0;
        const isActive = new Proxy({}, {
            set(target, prop, value) {
                const isSymbol = typeof prop === 'symbol';
                if (!isSymbol) {
                    debugger;
                    throw new TypeError('prop should be symbol!');
                }
                target[prop] = value;
                return true;
            }
        });
        let interval;
        const run = () => {
            dispatch(" ");
            interval = setInterval(
                () => {
                    Reflect.ownKeys(isActive).length
                        ? dispatch(dots[++i % dots.length] + " loading")
                        : (interval = clearInterval(interval))
                },
                120
            );
        };
        const dispatch = (detail) => this.ui.loading.innerText = detail;
        return {
            start(id) {
                !Reflect.ownKeys(isActive).length && run();
                isActive[id] = true;
                setTimeout(() => {
                    if (isActive[id]) {
                        alert(`request ${id.toString()} timeout`);
                        this.stop(id);
                    }
                }, 10000);
            },
            stop: (id) => {
                setTimeout(() => {
                    this.ui.loading.classList.add("invisible");
                    Reflect.deleteProperty(isActive, id);
                }, 500);
            },
        };
    }
}

customElements.define('ui-spinner', SpinnerComponent);
