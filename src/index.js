import "./style/app.scss";
import "./components";
import "./containers";
import "debuggerLib/footer";
import "debuggerLib/watch";
import { Routing, Helpers, Events } from "./utils";
import { CartService } from "./containers";
import {
  cartReducer,
  createState,
  itemsReducer,
  workersReducer,
  createStore
} from "./services";
import initTpl from './assets/init.html';

/**
 * UI elements
 */
const ui = Helpers.uiHandler(
  [".content"],
  [".nav"],
  [".cart-count"]
);

/**
 * Routing
 */
const routing = new Routing(ui.content, ui.nav);
routing.route("", Helpers.parseToHtml(initTpl).content.cloneNode(true));
routing.route("items", "<ui-items></ui-items>");
routing.route("workers", "<ui-workers></ui-workers>");
routing.route("cart", "<ui-cart></ui-cart>");

/**
 * Store
 */
export const Store = createStore({
  items: createState(itemsReducer, Object.freeze({ data: [], entities: {}, total: 0 })),
  workers: createState(workersReducer, Object.freeze({ data: [] })),
  cart: createState(
    cartReducer,
    Object.freeze({
      cart: [],
      shopping: [],
      discount: 0.9,
      tax: 1.23,
      totalCart: 0,
      totalShopping: 0,
    })
  ),
});

Store
  .pipe(store => store.cart)
  .subscribe(({ cart }) =>
    ui.cartCount.innerText = cart.reduce((acc, item) => acc + item.count, 0));

/**
 * Init cart
 */
document.addEventListener(Events.PAGE_LOADED, CartService.initCart);
