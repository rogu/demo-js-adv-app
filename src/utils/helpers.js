import { Events } from "./events";

export default class Helpers {
  static async(makeGenerator) {
    return function (...args) {
      var generator = makeGenerator.apply(this, args);

      function handle(result) {
        // result => { done: [Boolean], value: [Object] }
        if (result.done) return Promise.resolve(result.value);

        return Promise.resolve(result.value).then(
          function (res) {
            return handle(generator.next(res));
          },
          function (err) {
            return handle(generator.throw(err));
          }
        );
      }

      try {
        return handle(generator.next());
      } catch (ex) {
        return Promise.reject(ex);
      }
    };
  }

  static dashToCamelCase(str) {
    return str.replace(/(-\w)/g, (value) => value[1].toUpperCase());
  }

  static pipe(...fns) {
    return fns.reduceRight((prev, curr) => (...args) => prev(curr(...args)));
  }

  static uiHandler(...args) {
    const ui = {};
    function createEl(selector, eventType, ...fns) {
      let key;
      if (selector instanceof Node)
        ui[Helpers.dashToCamelCase(selector.nodeName.substring(1))] = selector;
      else {
        const isIdOrClass = /\.|#/.test(selector);
        key = Helpers.dashToCamelCase(
          isIdOrClass ? selector.substring(1) : selector
        );
        !ui[key] && (ui[key] = document.querySelector(selector));
      }
      try {
        eventType &&
          (key ? ui[key] : selector).addEventListener(eventType, (evt) => {
            let param = evt;
            fns.forEach((cb) => (param = cb(param)));
          });
      } catch (error) {
        debugger;
      }
    }
    args.forEach((item) => createEl(...item));
    return ui;
  }

  static mapObjectToParams(obj) {
    if (!obj) return "";
    return Object.keys(obj).reduce((acc, key) => {
      const el = key + "=" + obj[key];
      return acc + (acc ? "&" : "?") + el;
    }, "");
  }

  static parseToHtml(strTpl) {
    return new DOMParser()
      .parseFromString(strTpl, "text/html")
      .querySelector("template");
  }

  static arrayToEntities(arr) {
    return arr.reduce((acc, item) => ({ ...acc, [item.id]: item }), {});
  }

  static increaseParamValue(data, newItem, param) {
    const found = data.find((item) => item.id === newItem.id);
    return [
      ...(found
        ? data.map((item) =>
          item === found ? { ...found, [param]: found[param] + 1 } : item
        )
        : [...data, { ...newItem, [param]: 1 }]),
    ];
  }

  static decreaseParamValue(data, itemToDelete, param) {
    const found = data.find((item) => item.id === itemToDelete.id);
    return found[param] === 1
      ? data.filter((item) => item !== found)
      : data.map((item) =>
        item === found ? { ...found, [param]: found[param] - 1 } : item
      );
  }

  static remove(data, itemToDelete) {
    const found = data.find((item) => item.id === itemToDelete.id);
    return data.filter((item) => item !== found);
  }

  static loading1s() {
    const now = Symbol(Date.now());
    document.body.dispatchEvent(new CustomEvent(Events.LOADING_START, { detail: now }));
    setTimeout(() => {
      document.body.dispatchEvent(new CustomEvent(Events.LOADING_STOP, { detail: now }));
    }, 1000);
  }
}

Helpers.delay = (() => {
  let timer = {};
  return (cb, time = 500) => {
    const key = Symbol(cb);
    clearTimeout(timer[key]);
    timer[key] = setTimeout(() => cb(), time);
  };
})();
