import Helpers from "./helpers";

export const validateAction = action => {
    if (!action || typeof action !== "object" || Array.isArray(action)) {
        throw new Error("Action must be an object!");
    }

    if (typeof action.type === "undefined") {
        throw new Error("Action must have a type!");
    }
};

export const createStore = (store) => {
    Object.defineProperty(store, "dispatch", {
        enumerable: false,
        value: (action) => {
            validateAction(action);
            Object.keys(store).reduce((acc, name) => {
                const state = acc[name];
                state.value = state.reducer(state.value, action);
                store.listeners.forEach(listener => listener(
                    Object.keys(store).reduce((acc, name) => {
                        const state = acc[name];
                        return { ...acc, [name]: state };
                    }, store)
                ));
                return { ...acc, [name]: state };
            }, store);
        }
    });
    Object.defineProperty(store, "listeners", {
        enumerable: false,
        value: []
    });

    Object.defineProperty(store, "pipe", {
        enumerable: false,
        value: (...fns) => ({
            subscribe: (cb) => {
                store.listeners.push(() => {
                    const storeValues = Object.entries(store).reduce((acc, [key, { value }]) => ({ ...acc, [key]: value }), {});
                    cb(Helpers.pipe(...fns)(storeValues));
                });
            }
        })
    });

    Object.defineProperty(store, "getValue", {
        enumerable: false,
        value: () => Object.entries(store).reduce((acc, [key, { value }]) => ({ ...acc, [key]: value }), {})
    });

    Object.defineProperty(store, "subscribe", {
        enumerable: false,
        value: (fn) => store.listeners.push(fn)
    });

    return new Proxy(store, {
        get(target, prop) {
            if (!Reflect.has(target, prop)) {
                throw new TypeError(`state ${prop} not exists!`);
            }
            return target[prop];
        }
    });
};

export const createState = (reducer, initValue) => {
    const state = new Proxy({}, {
        set(target, prop, value) {
            const allowedProps = {
                value: { type: Object },
                reducer: { type: Function }
            };
            if (!allowedProps[prop]) {
                throw new TypeError(`you can use only these props: ${Reflect.ownKeys(allowedProps)}`);
            }
            if (!(value instanceof allowedProps[prop].type)) {
                throw new TypeError(`${prop} should be ${allowedProps[prop].type.name} but got ${typeof value}`);
            }
            target[prop] = value;
            return true;
        }
    });
    state.value = initValue;
    state.reducer = reducer;
    return state;
};
