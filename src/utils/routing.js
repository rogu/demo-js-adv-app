import { Events } from "./events";

export default class Routing {
  constructor(content, nav) {
    this.content = content;
    this.nav = nav;
    this.routes = {};
    this.run();
  }

  run() {
    window.onpopstate = (evt) => {
      if (evt.state) {
        while (evt.state.state) {
          evt = evt.state;
        }
      }
      this.loadContent(evt);
    };

    window.addEventListener(Events.LOAD, () => {
      this.loadContent({
        state: location.pathname.split("/").pop() || "default",
      });
    });

    this.nav.addEventListener(Events.CLICK, (evt) => {
      // close dropdown
      const elem = document.activeElement;
      elem?.blur();

      evt.preventDefault();
      evt.stopPropagation();
      const active = this.nav.querySelector(".btn-active");
      active && active.classList.remove("btn-active");
      const el = evt.target.closest && evt.target.closest("a");
      el.classList.add("btn-active");
      const href = el.getAttribute("href");
      href && this.go({ state: href });
    });
  }

  route(url, component) {
    this.routes[url || "default"] =
      component instanceof Node
        ? new XMLSerializer().serializeToString(component)
        : component;
  }

  loadContent({ state }) {
    const el =
      this.nav.querySelector(`[href="${state}"]`) ||
      this.nav.querySelectorAll("a")[0];
    el.classList.add("btn-active");
    this.content.innerHTML = this.routes[state]
      ? this.routes[state]
      : this.routes["default"];
    document.dispatchEvent(new CustomEvent(Events.PAGE_LOADED));
  }

  go(evt) {
    history.pushState({ state: evt.state }, evt.state, evt.state);
    this.loadContent(evt);
  }
}
