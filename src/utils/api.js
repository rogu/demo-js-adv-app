/**
 * api end points
 */
const
    API_BASE = 'https://api.debugger.pl/',
    API_AUTH_BASE = 'https://auth.debugger.pl/';

export const API = {
    ITEMS: `${API_BASE}items`,
    MANDATORY: `${API_BASE}items/mandatory`,
    WORKERS: `${API_BASE}workers`,
    LOG_IN: `${API_AUTH_BASE}login`,
    LOG_OUT: `${API_AUTH_BASE}logout`,
    IS_LOGGED_IN: `${API_AUTH_BASE}is-logged`
}
