export const Events = {
    INPUT: 'input',
    READY: 'ready',
    LOADING_START: 'loading-start',
    LOADING_STOP: 'loading-stop',
    SEARCH: 'search',
    SEARCH_RESET: 'search-reset',
    CHANGE: 'change',
    CLICK: 'click',
    LOAD: 'load',
    PAGE_LOADED: 'page loaded',
    SUBMIT: 'submit',
    MORE: 'MORE',
    BUY: 'BUY',
    ADD: '+',
    REMOVE: '-'
}
