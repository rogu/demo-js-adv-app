import { Helpers } from "../utils";
export const TYPES = {
    SET_ITEMS: 'SET_ITEMS',
    SET_WORKERS: 'SET_WORKERS',
    SET_CART: 'SET_CART'
};

export const itemsReducer = (state = { total: 0 }, action) => {
    switch (action.type) {
        case TYPES.SET_ITEMS:
            return {
                ...state,
                data: action.payload.data,
                total: action.payload.total,
                entities: Helpers.arrayToEntities(action.payload.data)
            };
        default:
            return state;
    }
};

export const workersReducer = (state = {}, action) => {
    switch (action.type) {
        case TYPES.SET_WORKERS:
            return {
                ...state,
                data: action.payload,
                entities: Helpers.arrayToEntities(action.payload)
            };
        default:
            return state;
    }
};

export const cartReducer = (state = {}, action) => {
    switch (action.type) {
        case TYPES.SET_CART:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
};
