import { Events, Helpers } from "../utils";

export class HttpBaseService {

    constructor(url) {
        this.url = url;
    }

    fetch(params, options, url = this.url) {
        const key = Symbol(url);
        document.body.dispatchEvent(new CustomEvent(Events.LOADING_START, { detail: key }));
        return fetch(url + Helpers.mapObjectToParams(params), {
            headers: { 'Content-Type': 'application/json', ...options }
        }).then((resp) => {
            Helpers.delay(() => document.body.dispatchEvent(new CustomEvent(Events.LOADING_STOP, { detail: key })));
            return resp.json();
        });
    }

    post(data, url = this.url) {
        const key = Symbol(url);
        document.body.dispatchEvent(new CustomEvent(Events.LOADING_START, { detail: key }));
        return fetch(url,
            {
                method: 'POST',
                body: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' },
                credentials: 'include'
            })
            .then((resp) => {
                Helpers.delay(() => document.body.dispatchEvent(new CustomEvent(Events.LOADING_STOP, { detail: key })));
                return resp.json();
            })
    }

}
