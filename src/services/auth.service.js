
import { API } from "../utils";
import { HttpBaseService } from "./http-base.service";

const secret = Symbol();

class AuthService extends HttpBaseService {

    constructor() {
        super();
        Object.defineProperty(this, "access", {
            get: () => this[secret]
        });
    }

    async login(userData) {
        const {data} = await super.post(userData, API.LOG_IN);
        this[secret] = !!data;
        localStorage.setItem('token', data.accessToken);
    }

    async isLogged() {
        const headers = { authorization: localStorage.getItem('token') };
        const data  = await super.fetch(null, headers, API.IS_LOGGED_IN);
        this[secret] = !data.warning;
        return this.access;
    }

    logout() {
        localStorage.removeItem('token');
        this[secret] = false;
    }
}

const instance = new AuthService();

export default instance;
