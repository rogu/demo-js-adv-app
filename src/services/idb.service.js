import { get, set, createStore } from 'idb-keyval';

const customStore = createStore('js-adv', 'cart');

export async function idbGetCart() {
    return get('cart', customStore);
}

export async function idbSetCart(payload) {
    return set('cart', payload, customStore );
}
